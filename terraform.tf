####################################################################
# VARIABLES
####################################################################

variable "aws_access_key" {}
variable "aws_secret_key" {}

# the path to the private key that corresponds to the key pair (key_name) in AWS
variable "private_key_path" {}

# refers to a key pair that exists within AWS so we can SSH into this instance once it's created
variable "key_name" {}

variable "region" {
	default = "us-east-1"
}


####################################################################
# PROVIDERS 
# - Here we're defining the AWS provider and feeding it the 
#   info we defined in variables further up.
# - We reference the variables as "var.name_of_variable"
# - Running "terraform init" will download the plugin for providers
####################################################################

provider "aws" {
	access_key = var.aws_access_key
	secret_key = var.aws_secret_key
	region     = var.region
}

####################################################################
# DATA 
# - Pulling data from a Provider
####################################################################

data "aws_ami" "aws-linux" {
	most_recent = true
	owners      = ["amazon"]

	filter {
		name   = "name"
		values = ["amzn-ami-hvm"]
	}

	filter {
		name   = "root-device-type"
		values = ["ebs"]
	}

	filter {
		name   = "virtualization-type"
		values = ["hvm"]
	}
}

####################################################################
# RESOURCES 
# - 
####################################################################

# This uses the default VPC within our region.
# It will NOT delete it on destroy.
resource "aws_default_vpc" "default" {

}

resource "aws_security_group" "allow_ssh" {
	name 		= "nginx_demo"
	description = "Allow ports for the nginx demo"
	vpc_id		= aws_default_vpc.default.id

	ingress {
		from_port	= 22
		to_port		= 22
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}

	ingress {
		from_port	= 80
		to_port		= 80
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}

	egress {
		from_port	= 0
		to_port		= 0
		protocol	= -1
		cidr_blocks	= ["0.0.0.0/0"]
	}
}

resource "aws_instance" "nginx" {
	ami						= data.aws_ami.aws-linux.id
	instance_type			= "t2.micro"
	key_name				= var.key_name
	vpc_security_group_ids	= [aws_security_group.allow_ssh.id]

	connection {
		type		= "ssh"
		host		= self.public_ip
		user		= "ec2-user"
		private_key	= file(var.private_key_path)
	}

	provisioner "remote-exec" {
		inline	= [
			"sudo yum install nginx -y",
			"sudo service nginx start"
		]
	}
}

####################################################################
# OUTPUT 
# - 
####################################################################

output "aws_instance_public_dns" {
	value = aws_instance.nginx.public_dns
}